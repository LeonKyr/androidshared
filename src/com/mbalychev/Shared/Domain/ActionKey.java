package com.mbalychev.Shared.Domain;

public final class ActionKey {
    public static String Title = "title";
    public static String Message = "message";
    public static String Datetime = "time-and-date";

    public static String MediaId = "mediaid";

    public static String SleepDuration = "sleep-duration";
    public static String SleepQuality = "sleep-quality";

    public static String MealType = "meal-type";
    public static String MealQuality = "meal-quality";

    public static String AbsenceReason = "absence-reason";
}
