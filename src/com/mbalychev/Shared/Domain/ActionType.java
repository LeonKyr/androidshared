package com.mbalychev.Shared.Domain;


import java.util.ArrayList;
import java.util.List;

public class ActionType {
    public static String Signin = "sign-in";
    public static String Signout = "sign-out";
    public static String Note = "note";
    public static String Photos = "photos";
    public static String Sleep = "sleep";
    public static String Meal = "meal";
    public static String Absence = "child-absence";
    public static String WillHaveMeal = "child-will-have-meal";

    public static String InternalNote = "internal-note";

    public static List<String> values = new ArrayList<String>(){{
        add(Signin);
        add(Signout);
        add(Note);
        add(Photos);
        add(Sleep);
        add(Meal);
        add(Absence);
        add(WillHaveMeal);
        add(InternalNote);
    }};
}