package com.mbalychev.Shared.Activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.*;
import android.media.MediaScannerConnection;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.widget.Toast;
import com.mbalychev.Shared.Helpers.ImageHelper;
import com.mbalychev.Shared.Helpers.UIHelper;
import com.mbalychev.Shared.R;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ImagePickerActivity extends Activity {
    private final int REQUEST_CAMERA = 43221;
    private final int SELECT_FILE = 41233;
    private final int THUMB_SIZE = 200;

    //private final int SELECT_FILE_OR_MAKE_PHOTO = 345;

    private Uri fileUri;
    private Boolean returnOnlyPath = false;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent inputIntent = getIntent();
        returnOnlyPath = inputIntent.getBooleanExtra("returnOnlyPath", false);

        //openImageIntent();
        final CharSequence[] items = {
                getString(R.string.takePhoto),
                getString(R.string.chooseFromLibrary),
                getString(R.string.cancel) };

        AlertDialog.Builder builder = new AlertDialog.Builder(ImagePickerActivity.this);
        builder.setTitle(getString(R.string.addPhoto));
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals(getString(R.string.takePhoto))) {
                    if(checkCameraHardware(ImagePickerActivity.this))
                    {
                        fileUri = getPhotoFileName(getApplicationContext());
                        if(fileUri != null)
                        {
                            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
                            ImagePickerActivity.this.startActivityForResult(intent, REQUEST_CAMERA);
                            //Toast.makeText(ImagePickerActivity.this, "Starting Image capture", Toast.LENGTH_LONG).show();
                    }   }
                    else{
                        UIHelper.showToast(getApplicationContext(), getString(R.string.noCamera), Toast.LENGTH_LONG, UIHelper.TDSuccess);
                    }
                }
                else if (items[item].equals(getString(R.string.chooseFromLibrary))) {
                    Intent intent = //new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        new Intent(Intent.ACTION_GET_CONTENT, android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI);

                    intent.setType("image/*");
                    ImagePickerActivity.this.startActivityForResult(Intent.createChooser(intent, getString(R.string.selectFile)), SELECT_FILE);
                    //Toast.makeText(ImagePickerActivity.this, "Starting Image selection", Toast.LENGTH_LONG).show();
                }
                else if (items[item].equals(getString(R.string.cancel))) {
                    dialog.dismiss();
                    setResult(RESULT_CANCELED);
                    finish();
                }
            }
        });
        builder.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            //Toast.makeText(ImagePickerActivity.this, "In ImagePickerActivity OnActivityResult. OK", Toast.LENGTH_LONG).show();

            Bitmap bitmap = null;
            String path = null;
            boolean isError = false;

            if (requestCode == REQUEST_CAMERA)
            {
                try {

                    //LastFileDetail detail = getFileDetails(fileUri, this);
                    //path = detail.FilePath;
                    //path = fileUri.getPath();
                    path = fileUri.getPath();

                    if(!returnOnlyPath){
                        //long id = ContentUris.parseId(fileUri);
                        //bitmap = MediaStore.Images.Thumbnails.getThumbnail(getContentResolver(), detail.Id, MediaStore.Images.Thumbnails.MICRO_KIND, null);
                        try {
                            bitmap = ThumbnailUtils.extractThumbnail(BitmapFactory.decodeFile(path),THUMB_SIZE, THUMB_SIZE);
                            bitmap = getCircularBitmap(bitmap);

                            String[] filesToScan = {path};
                            MediaScannerConnection.scanFile(getApplicationContext(), filesToScan, null, null);
                        } catch (Exception e) {
                            UIHelper.showToast(getApplicationContext(), "Failed to take Photo", Toast.LENGTH_LONG, UIHelper.TDError);
                            isError = true;
                        }
                    }

                } catch (Exception e) {
                    UIHelper.showToast(getApplicationContext(), "Failed to take Photo: ", Toast.LENGTH_LONG, UIHelper.TDError);
                    isError = true;
                }
            } else if (requestCode == SELECT_FILE) {
                Uri selectedImageUri = data.getData();

                try{
                    path = getFileFile(selectedImageUri);

                    if(!returnOnlyPath){
                        //long id = ContentUris.parseId(selectedImageUri);
                        bitmap = ThumbnailUtils.extractThumbnail(BitmapFactory.decodeFile(path),THUMB_SIZE, THUMB_SIZE);
                        //bitmap = MediaStore.Images.Thumbnails.getThumbnail(getContentResolver(), detail.Id, MediaStore.Images.Thumbnails.MICRO_KIND, null);

                        bitmap = getCircularBitmap(bitmap);
                    }
                }catch(Exception ex){
                    UIHelper.showToast(getApplicationContext(), "Failed to get Photo: "+ex.getMessage(), Toast.LENGTH_LONG, UIHelper.TDError);
                    isError = true;
                }
            }


            if(isError){
                setResult(RESULT_CANCELED);
            }else{
                Intent intent = new Intent();

                byte[] bytes = null;
                if(bitmap != null)
                    bytes = ImageHelper.getBytes(bitmap);

                intent.putExtra("bitmap_bytes", bytes);
                intent.putExtra("path", path);

                setResult(RESULT_OK, intent);
            }
        }
        else{
            //Toast.makeText(ImagePickerActivity.this, "In ImagePickerActivity OnActivityResult. Cancel", Toast.LENGTH_LONG).show();
            setResult(RESULT_CANCELED);
        }

        finish();
    }

    public static Bitmap getCircularBitmap(Bitmap bitmap) {
        Bitmap output;

        if (bitmap.getWidth() > bitmap.getHeight()) {
            output = Bitmap.createBitmap(bitmap.getHeight(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        } else {
            output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getWidth(), Bitmap.Config.ARGB_8888);
        }

        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());

        float r = 0;

        if (bitmap.getWidth() > bitmap.getHeight()) {
            r = bitmap.getHeight() / 2;
        } else {
            r = bitmap.getWidth() / 2;
        }

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawCircle(r, r, r, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);
        return output;
    }


    public String getFileFile(Uri uri) {
        Cursor cursor = getContentResolver().query(uri,
                new String[] { MediaStore.Images.ImageColumns.DATA },
                null, null, null);
        cursor.moveToFirst();
        String imageFilePath = cursor.getString(0);
        cursor.close();

        return imageFilePath;
    }

    public static File getPhotoFile(Context context){
        File photoFile = null;
        File outputDir = getPhotoDir(context);

        if(outputDir != null){
            String timeStamp = new SimpleDateFormat("yyyyMMDD HHmmss").format(new Date());
            String photoFileName = "IMG_"+timeStamp + ".png";

            photoFile = new File(outputDir, photoFileName);
        }

        return photoFile;
    }

    private static Uri getPhotoFileName(Context context){
        Uri photoFileUri = null;
        File file = getPhotoFile(context);

        if(file != null){
            photoFileUri = Uri.fromFile(file);
        }

        return photoFileUri;
    }

    private static File getPhotoDir(Context context){
        File outputDir = null;
        String externalStorageState = Environment.getExternalStorageState();
        if(externalStorageState.equals(Environment.MEDIA_MOUNTED)){
            File pictureDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
            outputDir = new File(pictureDir,"BooGooGoo");
            if(!outputDir.exists()){
                if(!outputDir.mkdirs()){
                    UIHelper.showToast(context, "Failed to create directory for Images", Toast.LENGTH_LONG, UIHelper.TDError);
                    outputDir = null;
                }
            }
        }

        return outputDir;
    }

    private boolean checkCameraHardware(Context context) {
        if (context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)){
            // this device has a camera
            return true;
        } else {
            // no camera on this device
            return false;
        }
    }

    class LastFileDetail{
        public String FilePath;
        public Long Id;
    }
}
    /*private void openImageIntent() {

// Determine Uri of camera image to save.
        fileUri = getPhotoFileName();

        // Camera.
        final List<Intent> cameraIntents = new ArrayList<Intent>();
        final Intent captureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        final PackageManager packageManager = getPackageManager();
        final List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for(ResolveInfo res : listCam) {
            final String packageName = res.activityInfo.packageName;
            final Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(packageName);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
            cameraIntents.add(intent);
        }

        // Filesystem.
        final Intent galleryIntent = new Intent();
        galleryIntent.setType("image/*");
        galleryIntent.setAction(Intent.ACTION_GET_CONTENT);

        // Chooser of filesystem options.
        final Intent chooserIntent = Intent.createChooser(galleryIntent, "Select Source");

        // Add the camera options.
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, cameraIntents.toArray(new Parcelable[]{}));

        startActivityForResult(chooserIntent, SELECT_FILE_OR_MAKE_PHOTO);
    }*/

    /*@Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if(resultCode == RESULT_OK)
        {
            if(requestCode == SELECT_FILE_OR_MAKE_PHOTO)
            {
                final boolean isCamera;
                if(data == null)
                {
                    isCamera = true;
                }
                else
                {
                    final String action = data.getAction();
                    if(action == null)
                    {
                        isCamera = false;
                    }
                    else
                    {
                        isCamera = action.equals(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    }
                }

                Bitmap bitmap = null;
                String path = null;

                if(isCamera)
                {
                    try {

                        //LastFileDetail detail = getFileDetails(fileUri, this);
                        //path = detail.FilePath;
                        //path = fileUri.getPath();
                        path = fileUri.getPath();

                        if(!returnOnlyPath){
                            //long id = ContentUris.parseId(fileUri);
                            //bitmap = MediaStore.Images.Thumbnails.getThumbnail(getContentResolver(), detail.Id, MediaStore.Images.Thumbnails.MICRO_KIND, null);
                            try {
                                bitmap = ThumbnailUtils.extractThumbnail(BitmapFactory.decodeFile(path),100, 100);
                                bitmap = getCircularBitmap(bitmap);

                                String[] filesToScan = {path};
                                MediaScannerConnection.scanFile(getApplicationContext(), filesToScan, null, null);
                            } catch (Exception e) {
                                UIHelper.showToast(getApplicationContext(), "Failed to take Photo: "+e.getMessage(), Toast.LENGTH_LONG, UIHelper.TDError);
                            }
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                else
                {
                    Uri selectedImageUri = data.getData();

                    try{
                        path = getFileFile(selectedImageUri);

                        if(!returnOnlyPath){
                            //long id = ContentUris.parseId(selectedImageUri);
                            bitmap = ThumbnailUtils.extractThumbnail(BitmapFactory.decodeFile(path),100, 100);
                            //bitmap = MediaStore.Images.Thumbnails.getThumbnail(getContentResolver(), detail.Id, MediaStore.Images.Thumbnails.MICRO_KIND, null);

                            bitmap = getCircularBitmap(bitmap);
                        }
                    }catch(Exception ex){
                        UIHelper.showToast(getApplicationContext(), "Failed to get Photo: "+ex.getMessage(), Toast.LENGTH_LONG, UIHelper.TDError);
                    }
                }

                Intent intent = new Intent();

                byte[] bytes = null;
                if(bitmap != null)
                    bytes = ImageHelper.getBytes(bitmap);

                intent.putExtra("bitmap_bytes", bytes);
                intent.putExtra("path", path);

                setResult(RESULT_OK, intent);
                finish();
            }
        }
    }*/
