package com.mbalychev.Shared.Helpers;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.util.DisplayMetrics;
import android.view.*;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.mbalychev.Shared.R;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public class UIHelper
{
    public static final int TDError = 1;
    public static final int TDSuccess = 2;
    public static final int TDInfo = 3;

    public static void showAsPopup(Activity activity) {
        //To show activity as dialog and dim the background, you need to declare android:theme="@style/PopupTheme" on for the chosen activity on the manifest
        activity.requestWindowFeature(Window.FEATURE_ACTION_BAR);
        activity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND,
                WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        WindowManager.LayoutParams params = activity.getWindow().getAttributes();
        params.height = 450; //fixed height
        params.width = 450; //fixed width
        params.alpha = 1.0f;
        params.dimAmount = 0.5f;
        activity.getWindow().setAttributes(params);
    }

    public static void showToast(Context context, String message, int duration, int type)
    {
        LayoutInflater inflater = LayoutInflater.from(context);
        View toastRoot = inflater.inflate(R.layout.toast, null);
        TextView toastMessage = (TextView)toastRoot.findViewById(R.id.toast_message);
        ImageView toastIcon = (ImageView)toastRoot.findViewById(R.id.toast_image);

        toastMessage.setText(message);

        Toast toast = new Toast(context);
        toast.setView(toastRoot);

        int gravity = Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL;
        switch (type)
        {
            case TDError:
                //gravity = Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL;
                toastIcon.setImageResource(R.drawable.pm_error);
                break;
            case TDSuccess:
                //gravity = Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL;
                toastIcon.setImageResource(R.drawable.pm_ok);
                break;
            case TDInfo:
                //gravity = Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL;
                toastIcon.setImageResource(R.drawable.pm_info);
                break;
        }
        toast.setGravity(gravity, 0, 0);
        toast.setDuration(duration);

        toast.show();
    }

    public static String getDateString(long milliSeconds, String dateFormat)
    {
        DateFormat formatter = new SimpleDateFormat(dateFormat);

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);

        return formatter.format(calendar.getTime());
    }

    public static Date getDate(int year, int month, int day, int hour, int minute) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, month);
        cal.set(Calendar.DAY_OF_MONTH, day);
        cal.set(Calendar.HOUR_OF_DAY, hour);
        cal.set(Calendar.MINUTE, minute);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    public static long getEndOfDay(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 999);
        return calendar.getTimeInMillis();
    }

    public static long getStartOfDay(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTimeInMillis();
    }

    public static String stringJoin(Collection<?> s, String delimiter) {
        StringBuilder builder = new StringBuilder();
        Iterator<?> iter = s.iterator();
        while (iter.hasNext()) {
            builder.append(iter.next());
            if (!iter.hasNext()) {
                break;
            }
            builder.append(delimiter);
        }
        return builder.toString();
    }

    public static boolean hasNetwork(Context context)
    {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }

    public static int getRandomNumber(){
        int min = 10000;
        int max = 99999;

        Random r = new Random();
        return r.nextInt(max - min + 1) + min;
    }

    public static String getDeviceUniqueId(){
        int suffix = getRandomNumber();

        String id = "35" + //we make this look like a valid IMEI
                Build.BOARD.length()%10+ Build.BRAND.length()%10 +
                Build.CPU_ABI.length()%10 + Build.DEVICE.length()%10 +
                Build.DISPLAY.length()%10 + Build.HOST.length()%10 +
                Build.ID.length()%10 + Build.MANUFACTURER.length()%10 +
                Build.MODEL.length()%10 + Build.PRODUCT.length()%10 +
                Build.TAGS.length()%10 + Build.TYPE.length()%10 +
                Build.USER.length()%10  //13 digits

                + "_"
                + String.valueOf(suffix);

        return  id;
    }

    public static long getCurrentTimeUnix() {
        return System.currentTimeMillis() / 1000L;
    }

    public static String getStringResourceByName(Context context, String aString) {
        // TODO: remove after fix
        if(aString.contains(".")){
            String[] parts = aString.split("\\.");
            aString = parts[0];
        }

        String packageName = context.getPackageName();
        int resId = context.getResources().getIdentifier(aString, "string", packageName);
        return context.getString(resId);
    }

    public static Drawable getDrawableResourceByName(Context context, String name){
        return context.getResources().getDrawable(
                context.getResources().getIdentifier(name,"drawable",context.getPackageName()));
    }

    public static int dpToPx(Context context, int dp) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        int px = Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
        return px;
    }

    public static int pxToDp(Context context, int px) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        int dp = Math.round(px / (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
        return dp;
    }
}
