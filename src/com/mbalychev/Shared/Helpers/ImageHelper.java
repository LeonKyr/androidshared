package com.mbalychev.Shared.Helpers;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.util.Base64;
import android.view.Display;
import android.view.WindowManager;

import java.io.*;

public class ImageHelper {

    public static Bitmap getBitmap(byte[] bytes){
        return BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
    }

    public static byte[] getBytes(Bitmap bitmap){
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        return stream.toByteArray();
    }

    public static String ToBase64(byte[] bytes){
        return new String(Base64.encode(bytes, Base64.DEFAULT));
    }

    public static byte[] FromBase64(String value){
        return Base64.decode(value.getBytes(), Base64.DEFAULT);
    }

    public static int getScreenWidth(Context context) {
        int columnWidth;
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();

        final Point point = new Point();
        try {
            display.getSize(point);
        } catch (java.lang.NoSuchMethodError ignore) { // Older device
            point.x = display.getWidth();
            point.y = display.getHeight();
        }
        columnWidth = point.x;
        return columnWidth;
    }

    public static Bitmap decodeFile(String filePath, int IMAGE_MAX_SIZE) {
        try {

            File f = new File(filePath);

            Bitmap b = null;

            //Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;

            FileInputStream fis = new FileInputStream(f);
            BitmapFactory.decodeStream(fis, null, o);
            fis.close();

            int scale = 1;
            if (o.outHeight > IMAGE_MAX_SIZE || o.outWidth > IMAGE_MAX_SIZE) {
                scale = (int)Math.pow(2, (int) Math.round(Math.log(IMAGE_MAX_SIZE /
                        (double) Math.max(o.outHeight, o.outWidth)) / Math.log(0.5)));
            }


            //Decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            fis = new FileInputStream(f);
            b = BitmapFactory.decodeStream(fis, null, o2);
            fis.close();

            return b;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static InputStream getStreamFromBitmap(Bitmap bm) throws IOException {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        InputStream is = new ByteArrayInputStream(stream.toByteArray());
        stream.close();
        return is;
    }

    public static String getImageStorageDir(){
        return android.os.Environment.getExternalStorageDirectory().toString();
    }

    /*public static Bitmap getImageThumbnail(String filePath, ContentResolver resolver, int kind){
        Bitmap thumbnail = null;
        try{
            File file = new File(filePath);
            Uri uri = Uri.fromFile(file);
            long id = ContentUris.parseId(uri);
            thumbnail = MediaStore.Images.Thumbnails.getThumbnail(resolver, id, kind, null);
        }catch(Exception e){
            e.printStackTrace();
        }

        return thumbnail;
    }*/
}
